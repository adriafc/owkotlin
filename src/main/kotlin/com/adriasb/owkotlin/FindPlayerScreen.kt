package com.adriasb.owkotlin

import com.adriasb.owkotlin.Styles.Companion.findErrorMessage
import com.adriasb.owkotlin.Styles.Companion.findErrorMessageHide
import com.adriasb.owkotlin.Styles.Companion.findFieldset
import com.adriasb.owkotlin.utils.Constants.Companion.FP_SCREEN_WINDOW_MIN_HEIGHT
import com.adriasb.owkotlin.utils.Constants.Companion.FP_SCREEN_WINDOW_WIDTH
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Label
import tornadofx.*

class FindPlayerScreen : View("Find player") {
    val mainController: MainController by inject()
    val errorLabel:Label

    private val model = object : ViewModel() {
        val battletag = bind { SimpleStringProperty() }
        val remember = bind { SimpleBooleanProperty() }
    }

    override val root = Form()

    init {

        with(root) {

            minWidth = FP_SCREEN_WINDOW_WIDTH
            minHeight = FP_SCREEN_WINDOW_MIN_HEIGHT

            maxWidth = FP_SCREEN_WINDOW_WIDTH
            maxHeight = FP_SCREEN_WINDOW_MIN_HEIGHT

            fieldset {
                field("Battetag") {
                    textfield(model.battletag) {
                        required()
                        whenDocked { requestFocus() }
                    }
                }
                field("Remember me") {
                    checkbox(property = model.remember)
                }
            }.addClass(findFieldset)

            errorLabel = label(mainController.messageError) {
                addClass(findErrorMessage, findErrorMessageHide)
            }

            button("Search") {
                isDefaultButton = true

                action {
                    model.commit {
                        mainController.findPlayer(
                            model.battletag.value,
                            model.remember.value
                        )
                    }
                }
            }

        }
    }

    override fun onDock() {
        model.validate(decorateErrors = false)
    }

    fun clear() {
        model.battletag.value = ""
        model.remember.value = false
    }
}