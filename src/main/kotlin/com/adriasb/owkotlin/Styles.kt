package com.adriasb.owkotlin

import com.adriasb.owkotlin.utils.Constants
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        // Define our styles
        val textBold by cssclass()
        val findWrapper by cssclass()
        val findWrapperGrow by cssclass()
        val findFieldset by cssclass()
        val findErrorMessage by cssclass()
        val findErrorMessageHide by cssclass()
        val levelImageVbox by cssclass()
        val infoHBoxStyle by cssclass()
    }

    init {
        textBold {
            fontWeight = FontWeight.BOLD
        }

        findWrapper {
            vgap = 7.px
            hgap = 10.px

            // backgroundColor += Color.DARKSLATEGRAY
        }

        findWrapperGrow {
            backgroundColor += Color.DARKSLATEGRAY
            fitToHeight = true
            fitToWidth = true

            maxHeight = 1000.px
            prefHeight = 1000.px
            minHeight = 1000.px
        }

        findFieldset {
            padding = box(0.px, 0.px, 4.px, 0.px)
        }

        findErrorMessage {
            //padding = box(4.px, 0.px, 12.px, 0.px)
            padding = box(0.px, 0.px, 14.px, 0.px)
            textFill = c("#ff0000")
        }

        findErrorMessageHide {
            maxHeight = 0.px
            prefHeight = 0.px
            minHeight = 0.px
        }

        levelImageVbox {

            maxWidth = 120.px
            prefWidth = 120.px
            minWidth = 120.px

            maxHeight = 120.px
            prefHeight = 120.px
            minHeight = 120.px



            // backgroundColor += Color.PURPLE
        }

        infoHBoxStyle {
            maxWidth = Constants.OV_SCREEN_WINDOW_WIDTH.px
            minWidth = Constants.OV_SCREEN_WINDOW_WIDTH.px
            prefWidth = Constants.OV_SCREEN_WINDOW_WIDTH.px

            backgroundColor += Color.MEDIUMPURPLE
        }
    }
}