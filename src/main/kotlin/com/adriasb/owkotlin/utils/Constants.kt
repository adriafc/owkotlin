package com.adriasb.owkotlin.utils

class Constants {
    companion object {

        const val FP_SCREEN_WINDOW_WIDTH = 300.0
        const val FP_SCREEN_WINDOW_MIN_HEIGHT = 120.0
        const val FP_SCREEN_WINDOW_MAX_HEIGHT = 175.0

        const val OV_SCREEN_WINDOW_WIDTH = 800.0
        const val OV_SCREEN_WINDOW_HEIGHT = 600.0
    }
}