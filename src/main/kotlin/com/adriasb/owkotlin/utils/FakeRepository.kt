package com.adriasb.owkotlin.utils

import com.adriasb.owkotlin.Player
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.io.File

class FakeRepository {

    var gson: Gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    val path = System.getProperty("user.dir")

    fun getFakeData(battletag: String): Player {

        val text = File(path.plus("/src/main/resources/adriasb-data.json")).readText()
        var player: Player = gson.fromJson(text, Player::class.java)
        player.battletag = battletag

        return player
    }

}