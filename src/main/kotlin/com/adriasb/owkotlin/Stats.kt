package com.adriasb.owkotlin

class Player(var battletag: String = "") {
    lateinit var stats: Stats
}

class Stats() {
    lateinit var quickplay: Quickplay
    lateinit var competitive: Competitive
}

class Competitive() {
    lateinit var gameStats: GameStats
    lateinit var overallStats: OverallStats
    lateinit var rollingAverageStats: RollingAverageStats
}

class Quickplay() {
    lateinit var gameStats: GameStats
    lateinit var overallStats: OverallStats
    lateinit var rollingAverageStats: RollingAverageStats
}

class RollingAverageStats(
    val soloKills: Double,
    val timeSpentOnFire: Double,
    val objectiveTime: Double,
    val objectiveKills: Double,
    val finalBlows: Double,
    val healingDone: Double,
    val deaths: Double,
    val eliminations: Double,
    val heroDamageDone: Double,
    val allDamageDone: Double,
    val barrierDamageDone: Double
)

class OverallStats(
    val endorsementSportsmanship: String,
    val prestigeImage: String,
    val endorsementLevel: Integer,
    val tierImage: String,
    val losses: Integer,
    val avatar: String,
    val prestige: Integer,
    val comprank: Integer,
    val level: Integer,
    val rankImage: String,
    val winRate: Integer,
    val games: Integer,
    val tier: String,
    val wins: Integer,
    val endorsementShotcaller: String,
    val endorsementTeammate: String
)

class GameStats(
    val medalsBronze: Double,
    val environmentalKillMostInGame: Double,
    val environmentalKills: Double,
    val killStreakBest: Double,
    val soloKillsMostInGame: Double,
    val medalsSilver: Double,
    val eliminationsMostInGame: Double,
    val allDamageDone: Double,
    val damageDone: Double,
    val soloKills: Double,
    val turretsDestroyed: Double,
    val defensiveAssists: Double,
    val finalBlows: Double,
    val teleporterPadDestroyedMostInGame: Double,
    val objectiveTime: Double,
    val multikillBest: Double,
    val eliminations: Double,
    val healingDoneMostInGame: Double,
    val timePlayed: Double,
    val medals: Double,
    val objectiveKills: Double,
    val objectiveKillsMostInGame: Double,
    val meleeFinalBlowsMostInGame: Double,
    val meleeFinalBlows: Double,
    val heroDamageDoneMostInGame: Double,
    val objectiveTimeMostInGame: Double,
    val defensiveAssistsMostInGame: Double,
    val heroDamageDone: Double,
    val medalsGold: Double,
    val gamesWon: Double,
    val kpd: Double,
    val healingDone: Double,
    val timeSpentOnFire: Double,
    val barrierDamageDone: Double,
    val finalBlowsMostInGame: Double,
    val reconAssists: Double,
    val reconAssistsMostInGame: Double,
    val deaths: Double,
    val multikills: Double,
    val turretsDestroyedMostInGame: Double,
    val barrierDamageDoneMostInGame: Double,
    val teleporterPadsDestroyed: Double,
    val offensiveAssists: Double,
    val allDamageDoneMostInGame: Double,
    val offensiveAssistsMostInGame: Double,
    val cards: Double,
    val timeSpentOnFireMostInGame: Double
)