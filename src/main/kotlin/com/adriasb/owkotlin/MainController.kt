package com.adriasb.owkotlin

import com.adriasb.owkotlin.utils.Constants.Companion.FP_SCREEN_WINDOW_MAX_HEIGHT
import com.adriasb.owkotlin.utils.Constants.Companion.FP_SCREEN_WINDOW_MIN_HEIGHT
import com.adriasb.owkotlin.utils.Constants.Companion.FP_SCREEN_WINDOW_WIDTH
import com.adriasb.owkotlin.utils.FakeRepository
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import tornadofx.*

class MainController : Controller() {
    val findPlayerScreen: FindPlayerScreen by inject()
    val overviewScreen: OverviewScreen by inject()
    val fakeRepository: FakeRepository = FakeRepository()

    val messageError = SimpleStringProperty()

    fun init() {
        with(config) {
//            showFindPlayerScreen("")
//            findPlayer("admin", true)
//            if (containsKey(BATTLETAG))
//                findPlayer(string(BATTLETAG), true)
//            else
            showFindPlayerScreen("")
        }
    }

    fun showFindPlayerScreen(message: String) {
        messageError.set(message)
        overviewScreen.replaceWith(findPlayerScreen, sizeToScene = true, centerOnScreen = true)
    }

    fun showOverviewScreen() {
        findPlayerScreen.replaceWith(overviewScreen, sizeToScene = true, centerOnScreen = true)
    }

    fun findPlayer(battletag: String, remember: Boolean) {
        runAsync {
            battletag == "admin"
        } ui { successfulLogin ->
            if (successfulLogin) {
                findPlayerScreen.clear()
                findPlayerScreen.errorLabel.addClass(Styles.findErrorMessageHide)

                var player:Player = fakeRepository.getFakeData(battletag)
                overviewScreen.bindValuesToElements(player)

                if (remember) {
                    with(config) {
                        set(BATTLETAG to battletag)
                        set(REMEMBER to remember)
                        save()
                    }
                }

                findPlayerScreen.setWindowMinSize(FP_SCREEN_WINDOW_WIDTH, FP_SCREEN_WINDOW_MIN_HEIGHT)
                showOverviewScreen()
            } else {
                findPlayerScreen.errorLabel.removeClass(Styles.findErrorMessageHide)

                // Modifica la altura del window cuando hay un error
                findPlayerScreen.setWindowMinSize(FP_SCREEN_WINDOW_WIDTH, FP_SCREEN_WINDOW_MAX_HEIGHT)
                showFindPlayerScreen("Player not found. Please try again.")
            }
        }
    }

    fun logout() {
        with(config) {
            remove(BATTLETAG)
            save()
        }

        showFindPlayerScreen("Log in as another user")
    }

    companion object {
        val BATTLETAG = "Example#1234"
        val REMEMBER = "false"
    }
}

