package com.adriasb.owkotlin

import com.adriasb.owkotlin.Styles.Companion.infoHBoxStyle
import com.adriasb.owkotlin.Styles.Companion.textBold
import com.adriasb.owkotlin.utils.Constants
import com.sun.org.apache.xalan.internal.lib.ExsltStrings
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import javafx.scene.text.TextAlignment
import tornadofx.*
import javax.swing.GroupLayout

class OverviewScreen : View("Secure Screen") {
    val mainController: MainController by inject()
    var levelImageVbox:VBox = VBox()

    override val root = BorderPane()

    val battletag = SimpleStringProperty()
    val avatar = SimpleStringProperty()
    val prestige = SimpleIntegerProperty()
    val comprank = SimpleIntegerProperty()
    val level = SimpleIntegerProperty()
    val rankImage = SimpleStringProperty()
    val tier = SimpleStringProperty()
    val prestigeImage = SimpleStringProperty()
    val tierImage = SimpleStringProperty()

    init {

        with(root) {
            setPrefSize(Constants.OV_SCREEN_WINDOW_WIDTH, Constants.OV_SCREEN_WINDOW_HEIGHT)

            // Main Container
            center = vbox {

                // Info
                hbox {
                    addClass(infoHBoxStyle)
                    alignment = Pos.CENTER

                    // Battletag
                    hbox {
                        alignment = Pos.CENTER

                        imageview(avatar) {
                            alignment = Pos.CENTER
                            isPreserveRatio = true
                            fitHeight = 70.0
                        }

                        label(battletag)
                    }

                    // LevelImage & Level
                    levelImageVbox = vbox {
                        addClass(Styles.levelImageVbox)

                        hbox {
                            minHeight = 70.0
                            alignment = Pos.BOTTOM_CENTER

                            label(level) {
                                alignment = Pos.TOP_CENTER
                                style {
                                    fontWeight = FontWeight.BOLD
                                }
                            }
                        }

                        hbox {
                            minHeight = 55.0

                            imageview(prestigeImage) {
                                alignment = Pos.BOTTOM_CENTER
                                isPreserveRatio = true
                                fitHeight = 40.0
                            }
                        }
                    }

                    // PrestigeImage & Comprank
                    vbox {

                        imageview(tierImage) {
                            alignment = Pos.CENTER
                            isPreserveRatio = true
                            fitHeight = 40.0
                        }

                        label(comprank).style {
                            fontWeight = FontWeight.BOLD
                        }
                    }

                }

            }

            center.style {
                backgroundColor += c("#dfe7f4")
            }

        }
    }

    fun bindValuesToElements(player: Player) {
        battletag.set(player.battletag.toUpperCase())
        prestige.set(player.stats.competitive.overallStats.prestige.toInt())
        comprank.set(player.stats.competitive.overallStats.comprank.toInt())
        level.set(player.stats.competitive.overallStats.level.toInt())
        tier.set(player.stats.competitive.overallStats.tier)
        avatar.set(player.stats.competitive.overallStats.avatar.trim())
        rankImage.set(player.stats.competitive.overallStats.rankImage.trim())
        prestigeImage.set(player.stats.competitive.overallStats.prestigeImage.trim())
        tierImage.set(player.stats.competitive.overallStats.tierImage.trim())

        // Changing Background
        var image = Image(player.stats.competitive.overallStats.rankImage.trim())
        var bgImage = BackgroundImage(
            image,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            BackgroundSize(120.0, 120.0, false, false, false, false)
        )
        levelImageVbox.background = Background(bgImage)
    }
}