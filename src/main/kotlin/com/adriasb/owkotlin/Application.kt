package com.adriasb.owkotlin

import com.adriasb.owkotlin.utils.FakeRepository
import javafx.scene.layout.Priority
import javafx.stage.Stage
import tornadofx.*

class Application : App(FindPlayerScreen::class, Styles::class) {
    val mainController: MainController by inject()
    val fakeRepository: FakeRepository = FakeRepository()

    override fun start(stage: Stage) {
        super.start(stage)

        stage.isResizable = false

        stage.vbox {
            vgrow = Priority.ALWAYS
        }
        mainController.init()
    }
}

fun main(args: Array<String>) {
    launch<Application>(args)
}